from django.contrib import admin
from portfolio.models import Project, Contact, AboutSection


# Register your models here.
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title', 'year', 'vimeo_id', 'order')
    class Meta:
        ordering = ['-order']


# @admin.register(GalleryItem)
# class GalleryItemAdmin(admin.ModelAdmin):
#     list_display = ('title', 'order')


@admin.register(AboutSection)
class AboutSectionsAdmin(admin.ModelAdmin):
    pass


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    pass
