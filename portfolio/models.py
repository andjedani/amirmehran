from django.db import models
from tinymce import HTMLField

PROJECT_LABEL_CHOICES = (
    ('m', 'Motion'),
    ('a', 'Animation'),
    ('p', 'Print')
)


class Project(models.Model):
    vimeo_id = models.CharField(max_length=30, null=True, blank=True)
    title = models.CharField(max_length=255, null=False, blank=False)
    year = models.SmallIntegerField(blank=True)
    short_description = models.TextField(null=True, blank=True)
    description = HTMLField('Description', null=True, blank=True)
    thumbnail = models.ImageField(upload_to='thumbs/', default='assets/placeholder.png')
    image = models.ImageField(upload_to='project_images/', default='assets/placeholder.png', null=True, blank=True)
    poster = models.ImageField(upload_to='project_poster/', default='assets/placeholder.png')
    order = models.SmallIntegerField(default=-1)
    label = models.CharField(max_length=1, choices=PROJECT_LABEL_CHOICES, default='m')


class Contact(models.Model):
    content = HTMLField('Content')


class AboutSection(models.Model):
    title = models.CharField(max_length=80, null=False, blank=False)
    description = HTMLField('Description')
    image = models.ImageField(upload_to='about_images/', default='assets/placeholder.png')
    show_order = models.IntegerField(default=-1)

