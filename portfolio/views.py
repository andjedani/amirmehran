from django.shortcuts import render

from .models import Contact, Project, AboutSection


def index_view(request):
    return render(request, 'index.html')


def about_view(request):
    about_sections = list(AboutSection.objects.all().order_by('-show_order'))
    context = {'sections': about_sections}
    print(context)
    return render(request, 'about.html', context)


def prints_view(request):
    prints = list(Project.objects.filter(label='p').order_by('-order'))
    context = {'prints': prints}
    return render(request, 'print.html', context)


def print_item(request, print_title):
    print_item_data = Project.objects.filter(title=print_title)[0]
    context = {'print': print_item_data}
    return render(request, 'PrintItem.html', context)


def motions_view(request):
    motions = list(Project.objects.filter(label='m').order_by('-order'))
    context = {'motions': motions}
    return render(request, 'motion.html', context)


def motion_item(request, motion_title):
    motion = Project.objects.filter(title=motion_title)[0]
    context = {'motion': motion}
    return render(request, 'MotionItem.html', context)


def animations_view(request):
    animations = list(Project.objects.filter(label='a').order_by('-order'))
    context = {'animations': animations}
    return render(request, 'animation.html', context)


def animation_item(request, animation_title):
    animation = Project.objects.filter(title=animation_title)[0]
    context = {'animation': animation}
    return render(request, 'AnimItem.html', context)


def contact_view(request):
    contact = Contact.objects.all()[0]
    context = {'content': contact.content}
    return render(request, 'contact.html', context)
