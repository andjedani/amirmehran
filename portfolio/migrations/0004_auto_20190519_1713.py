# Generated by Django 2.2 on 2019-05-19 17:13

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0003_contact_galleryitem'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutSection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=80)),
                ('description', tinymce.models.HTMLField(verbose_name='Description')),
                ('image', models.ImageField(default='assets/placeholder.png', upload_to='about_images/')),
            ],
        ),
        migrations.AlterField(
            model_name='galleryitem',
            name='image',
            field=models.ImageField(default='assets/placeholder.png', upload_to='images/'),
        ),
        migrations.AlterField(
            model_name='project',
            name='thumbnail',
            field=models.ImageField(default='assets/placeholder.png', upload_to='thumbs/'),
        ),
    ]
