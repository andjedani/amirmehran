"""amirmehran URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from portfolio.views import animations_view, animation_item, contact_view, about_view, index_view, motion_item, motions_view, \
    print_item, prints_view
from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
                  path('', index_view, name='index'),
                  path('admin/', admin.site.urls),
                  path('about/', about_view, name='about'),
                  url(r'^prints/(?P<print_title>.+?)/$', print_item, name='print_item'),
                  url(r'^print/(?P<print_title>.+?)/$', print_item, name='print_item'),
                  path('prints/', prints_view, name='prints'),
                  url(r'^motion/(?P<motion_title>.+?)/$', motion_item, name='motion_item'),
                  path('motion/', motions_view, name='motions'),
                  url(r'^motions/(?P<motion_title>.+?)/$', motion_item, name='motion_item'),
                  path('motions/', motions_view, name='motions'),
                  url(r'^animations/(?P<animation_title>.+?)/$', animation_item, name='animation_item'),
                  path('animations/', animations_view, name='animations'),
                  path('contact/', contact_view, name='contact'),
                  url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
                  url(r'^tinymce/', include('tinymce.urls')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)  # + \
# static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += staticfiles_urlpatterns()
